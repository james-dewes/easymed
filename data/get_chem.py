import xml.etree.ElementTree as ET
import sys

tree = ET.parse('drugbank.xml')
root = tree.getroot()

for drug in root:
    for child in drug:
        if child.tag == "{http://www.drugbank.ca}drugbank-id":
            if child.get("primary") == "true":
                sys.stdout.write(child.text + ', ')
        if child.tag == "{http://www.drugbank.ca}name":
            print child.text.encode("ascii", "ignore").decode("ascii")
