import xml.etree.ElementTree as ET
import sys

tree = ET.parse('drugbank.xml')
root = tree.getroot()

for drug in root:
    for child in drug:
        if child.tag == "{http://www.drugbank.ca}drugbank-id":
            if child.get("primary") == "true":
                # sys.stdout.write(child.text + ', ')
                drug_id = child.text
        if child.tag == "{http://www.drugbank.ca}drug-interactions":
            for grandchild in child:
                if grandchild.tag == "{http://www.drugbank.ca}drug-interaction":
                    react_drug_id = ""
                    react_description = ""
                    for grandgrandchild in grandchild:
                        if grandgrandchild.tag == "{http://www.drugbank.ca}drugbank-id":
                           react_drug_id =  grandgrandchild.text
                        if grandgrandchild.tag == "{http://www.drugbank.ca}description":
                            react_description = grandgrandchild.text
                    print drug_id + ", " + react_drug_id + ", " + react_description
