<!doctype html>
<html lang="en-US">
	
	<!-- code adapted from http://designshack.net -->	 
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <title>EasyMed Demo</title>
  <!-- to be replaced <link rel="shortcut icon" href="http://designshack.net/favicon.ico"> -->
  <link rel="stylesheet" type="text/css" media="all" href="../styles/main.css">
  <script type="text/javascript" src="//druginfo.x10host.com/scripts/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="//druginfo.x10host.com/scripts/jquery.autocomplete.min.js"></script>
  <script type="text/javascript" src="//druginfo.x10host.com/scripts/med-autocomplete.js"></script>
  <script type="text/javascript" src="//scripts/font-size.js"></script>
</head>

<body>
  <header>
	<img src="//druginfo.x10host.com/images/logo.jpg"/>
	<div class="topright">
	<p><a href="signin.php">Log In/Sign Up</a>
	<a class="increaseFont" href="#"><sub>A</sub>A</a>
	<a class="decreaseFont" href="#">A<sub>A</sub></a></p>
	
        	<form><input type="text" name="medication" class="smallinput" id="autocomplete" value="Search for medication"><input type="button" value=" Search "  class="smallinput" name="topsearch" /></form>
      	</div>
  </header>
  <nav>
	<ul>
		<li>Medication Finder</li>
		<li>About Us</li>
		<li>Support</li>
		<li>Sponsors</li>
		<li>Help</li>
		<li>Contact us</li>
	</ul>
  </nav>

  <div id="w">

    <div id="content">
