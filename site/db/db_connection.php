<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


function getConn()
{	
  $credentials = array(
	  'driver' => 'mysql',
	  'persistent' => false,
	  'host' => 'localhost',
	  'login' => 'public',
	  'password' => 'nhshackday',
	  'database' => 'easymed',
	  'prefix' => 'druginfo',
	  //'encoding' => 'utf8',
  );
  $conn = new mysqli( $credentials['host'], $credentials['login'], $credentials['password'] );

  if ($conn->connect_error) {
    die("Connection Failed:" . $conn->connect_error);
  }

  $conn->select_db('druginfo_easymed');

  return $conn;
}

?>


