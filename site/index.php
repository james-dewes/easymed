<?php include('elements/header.php'); ?>
	  <div id="topbar">
		  <p>Demo only<br/> This is not a live website and should not be relied on for medical information.</p>
	  </div>
      
      <div id="searchfield">
        <form><input type="text" name="medication" class="biginput" id="autocomplete"></form>
      </div><!-- @end #searchfield -->
      
      <div id="outputbox">
        <p id="outputcontent">Start typing and suggested medication names will appear here.</p>
      </div>
<?php include('elements/footer.php'); ?>
