<?php require('controllers/medicationinfo.php');
$medication = getMedicationInfo($_GET['id']);

var_dump($medication);
?>
<?php include('elements/header.php'); ?>
<h1><?php echo $medication['name'] ?></h1>
<?php 
$path = '../images/' . $medication['id'] . '/';
var_dump($path);
if ( file_exists($path)) {
$pics = scandir($path);
var_dump($pics);
for($i = 2; $i<count($pics); $i++) { ?>
<img src="../images/<?php echo $medication['id'] . '/' . $pics[$i] ?>" />
<?php } }?>
<h2>Ingredients</h2>
<ul>
<?php for($i = 0; $i<count($medication['ingredients']); $i++) { ?>
<li class="ingredient" data-id="<?php echo $i ?>"><?php echo $medication['ingredients'][$i]['name'] ?></li>
<?php } ?>
</ul>
<h2>Known to Interact With</h2>
<ul>
<?php for($i = 0; $i<count($medication['interactions']); $i++) { ?>
<li><?php echo $medication['interactions'][$i]['name'] ?></li>
<?php } ?>
</ul>
<?php for($i = 0; $i<count($medication['ingredients']); $i++) { ?>
<div class="ingredient-popover" id="ingredient<?php echo $i ?>">
<dl>
<dd>Name:</dd>
<dt><?php echo $medication['ingredients'][$i]['name'] ?></dt>
<dd>Pregnancy Risk:</dd>
<dt><?php echo $medication['ingredients'][$i]['pregnancy_risk'] ?></dt>
<dd>Alcohol OK:</dd>
<dt><?php echo $medication['ingredients'][$i]['alcohol_ok'] ?></dt>
<dd>Side Effects:</dd>
<dt><?php echo $medication['ingredients'][$i]['side_effects'] ?></dt>
<dd>More info:</dd>
<dt><a href="http://drugbank.ca/drugs/<?php echo $medication['ingredients'][$i]['name'] ?>">DrugBank</a></dt>
</dl>
<?php } ?>

<script>
//$('.ingredient-popover').hide();
$('li.ingredient').on('click', function() {
  $('#ingredient' + $(this).data('id') ).toggle();
});
</script>
<?php include('elements/footer.php'); ?>

