$(function() {
  $('#autocomplete').autocomplete( {
    'serviceUrl': 'controllers/autocomplete.php',
    'onSelect': function (suggestion) {
        var thehtml = '<strong>Drug name:</strong> ' + suggestion.value + ' <br> <strong>Drug ID:</strong> ' + suggestion.data;
	      $('#outputcontent').html(thehtml);
      }
  });
});
