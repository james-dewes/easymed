<?php 
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require($_SERVER['DOCUMENT_ROOT'] .'/db/db_connection.php');

function getMedicationInfo( $medicationid ) {
  $conn = getConn();
  $medicationQuery = "SELECT * FROM medication WHERE medication_id=$medicationid;";
  $ingredientsQuery = "SELECT i.*, GROUP_CONCAT(ia.name ORDER BY ia.name DESC SEPARATOR ',\n') AS aliases " .
                               "FROM medication_ingredients mi " .
                               "JOIN ingredients i " .
                                   "ON i.ingredient_id = mi.ingredient_id " .
                               "JOIN ingredient_alias ia " .
	                               "ON i.ingredient_id = ia.ingredient_id " .
                               "WHERE medication_id=$medicationid " .
                               "GROUP BY ia.ingredient_id;";
  $ingredientInteractionQuery = "SELECT i.* " .
                                "FROM ingredient_interaction ii " .
                                "JOIN ingredients i " .
                                "ON i.ingredient_id = ii.ingredient2_id " .
                                "WHERE ii.ingredient1_id IN ( " .
                                "SELECT mi.ingredient_id " .
                                "FROM medication_ingredients mi " .
                                "WHERE medication_id = $medicationid );";
  $conn->multi_query( $medicationQuery . $ingredientsQuery . $ingredientInteractionQuery );
  $result = $conn->store_result();
  
  if ($result && $result->num_rows == 1) {
      $row = $result->fetch_assoc();
      $array = array(
          'id' => $row['medication_id'], 
          'name' => $row['name'],
          'description' => $row['description'],
          );
  }
  else {
    echo 'no results';
  }
  
  $conn->next_result();
  $ingredientsResult = $conn->store_result();
  if ($ingredientsResult && $ingredientsResult->num_rows > 0) {
      $ingredients = array();
      while($row = $ingredientsResult->fetch_assoc()) {
          array_push($ingredients, array( 
          'name' => $row['name'], 
          'drug_bank_id' => $row['drug_bank_id'],
          'pregnancy_risk' => $row['pregnancy_risk'],
          'alcohol_ok' => $row['alcohol_ok'],
          'side_effects' => $row['side_effects'],
          'aliases' => $row['aliases']));
      }
      $array['ingredients'] = $ingredients;
  }
  else {
    echo 'no results';
  }
  
  $conn->next_result();
  $ingredient_interactions = $conn->store_result();
  if ( $ingredient_interactions && $ingredient_interactions->num_rows > 0 ) {
    $interactions = array();
    while($row = $ingredient_interactions->fetch_assoc()) {
      array_push($interactions, array( 
        'name' => $row['name'],
        'drug_bank_id' => $row['drug_bank_id'],
        'pregnancy_risk' => $row['pregnancy_risk'],
        'alcohol_ok' => $row['alcohol_ok'],
        'side_effects' => $row['side_effects']) );
    }
    $array['interactions'] = $interactions;
  }
  else {
    echo 'no results';
  }
  
  return $array;
}

//$ans = getMedicationInfo(8);
//var_dump($ans);
?>
