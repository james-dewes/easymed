<?php 
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require('../db/db_connection.php');

function getUserInfo( $userid ) {
  $conn = getConn();
  $userQuery = "SELECT * FROM users WHERE user_id='$userid';";
  $restrictedIngredientsQuery = "SELECT i.name, i.drug_bank_id FROM user_restricted_ingredients uri JOIN ingredients i ON uri.ingredient_id = i.ingredient_id WHERE user_id = '$userid';";
  $medicationUsageQuery = "SELECT name FROM medication_usage mu JOIN medication m ON mu.medication_id = m.medication_id WHERE mu.user_id = '$userid';";
  $conn->multi_query( $userQuery . $restrictedIngredientsQuery . $medicationUsageQuery );
  
  $result = $conn->store_result();
  if ($result && $result->num_rows == 1) {
      $row = $result->fetch_assoc();
      $array = array(
          'id' => $row['user_id'], 
          'year_of_birth' => $row['year_of_birth'],
          'sex' => $row['sex'],
          'height' => $row['height'],
          'weight' => $row['weight'],
          );
  }
  else {
    //echo 'no results';
  }

  $conn->next_result();
  $restricted_ingredients = $conn->store_result();
  if ($restricted_ingredients && $restricted_ingredients->num_rows > 0) {
      $ingredients = array();
      while($row = $restricted_ingredients->fetch_assoc()) {
          array_push($ingredients, array( 'name' => $row['name'], 'drug_bank_id' => $row['drug_bank_id']));
      }
      $array['user_restricted_ingredients'] = $ingredients;
  }
  else {
    // echo 'no results for restricted ingredients';
  }
  
  $conn->next_result();
  $medication_usage = $conn->store_result();
  if ( $medication_usage && $medication_usage->num_rows > 0 ) {
    $meds = array();
    while($row = $medication_usage->fetch_assoc()) {
      array_push($meds, array( 'name' => $row['name'] ) );
    }
    $array['meds'] = $meds;
  }
  else {
    //echo 'no results';
  }
  
  return $array;
}

$ans = getUserInfo('david');
var_dump($ans);
?>
